import 'package:flutter/material.dart';

class DrawingArea {
  Offset point;
  Paint areaPoint;
  DrawingArea({required this.point, required this.areaPoint});
}
