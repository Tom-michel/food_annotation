

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class ImageLoad extends StatefulWidget {
  const ImageLoad({Key? key}) : super(key: key);

  @override
  State<ImageLoad> createState() => _ImageLoadState();
}

class _ImageLoadState extends State<ImageLoad> {
  File? image;
  Future pickImage(ImageSource source) async {
    try{
      final image = await ImagePicker().pickImage(source: source);
      if (image ==null)return;
      final imageTemporary =  File(image.path);
      setState(() {
        this.image = imageTemporary;
      });
    }on PlatformException catch(e){
        if (kDebugMode) {
          print('Failed to pick image $e');
        }
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber.shade300,
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: [
            Spacer(),
            image != null? Image.file(image!, width: 160, height: 160) : const FlutterLogo(size:160),
            const SizedBox(height: 24),
            const Text(
              'Food Anotations',
            ),
            const SizedBox(height: 24),
            ElevatedButton(
                child: Row(
                  children: [
                    TextButton(

                      child: IconButton(
                        color: Colors.white,

                        icon: Icon(Icons.image_outlined),
                        onPressed: () {},
                      ),
                      onPressed: () {},
                    ),
                    Text("Pick Galery"),
                  ],
                ),
                onPressed: () => pickImage(ImageSource.gallery),
            ),
            const SizedBox(height: 24),
            ElevatedButton(
              child: Row(

                children: [
                  TextButton(
                    child: IconButton(
                      color: Colors.white,
                      icon: Icon(Icons.camera_alt_outlined),
                      onPressed: () {},
                    ),
                    onPressed: () {},
                  ),
                  Text("Pick camera"),
                ],
              ),
              onPressed: () => pickImage(ImageSource.camera),
            ),
            Spacer(),
          ],
        ),
      )
    ) ;
  }


}
