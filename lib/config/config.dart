import 'package:flutter/painting.dart';

class Config {
  static final colors = _Color();
  static final assets = _Asset();
}

class _Color {
  final primaryColor = Color(0xFF1F8D2A);
  final secondColor = Color(0xFFFAE200);
  final thirdColor = Color(0xFFFFEB3B);
  final primaryTextColor = Color(0xFFFFFFFF);
  final secondTextColor = Color(0xFF000000);
}

class _Asset {
  final splash_img = "assets/images/Splash_screen.png";
  final splash_bg_img = "assets/images/CLIPART.png";
  final plat = "assets/images/plat.png";
  final logo = "assets/images/LOGO.png";
  final logo2 = "assets/images/LOGO2.png";
  final bar = "assets/images/bottombar.png";
  final btnUpload = "assets/images/btnupload.png";
  final btnCapture = "assets/images/Btncapture.png";
  final sideTop = "assets/images/sidetop.png";
}
