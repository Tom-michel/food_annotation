import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:food_annotation/config/config.dart';
import 'package:food_annotation/screens/home.dart';
import 'package:food_annotation/screens/newPaint.dart';
import 'package:food_annotation/screens/splash_screen.dart';
import 'package:food_annotation/widgets/splash.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  // initialize hive
  await Hive.initFlutter();
  var box = await Hive.openBox('recent_image');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        title: "Food Annotation",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Config.colors.secondColor,
          primarySwatch: Colors.blue,
        ),
        routes: {
          '/': (context) => SplashWidget(
                nextPage: const Home(),
                time: 5,
                child: SplashScreen(),
              ),
          '/home': (context) => const Home(),
          '/paint': (context) => Demo(),
        }
        /* home: SplashWidget(
        child: SplashScreen(),
        nextPage: const Home(),
        time: 20,
      ),*/
        );
  }
}
