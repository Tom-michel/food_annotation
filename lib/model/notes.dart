final String TableNotes = 'notes';

class NoteFields{
  static final List<String> column = [
   id,
   isImportant ,
   number,
   title ,
   description ,
    createTime ,
  ];
  static final String id = '_id';
  static final String isImportant = 'isImportant';
  static final String number = 'number';
  static final String title = 'title';
  static final String description = 'description';
  static final String createTime = 'createTime';
}

class Note {
  final int? id;
  final bool isImportant;
  final int number;
  final String title;
  final String description;
  final DateTime createTime;

   Note({
    this.id,
     required this.isImportant,
     required this.number,
     required this.title,
     required this.description,
     required this.createTime,
});

   Map<String, Object?> toJson() => {
     NoteFields.id: this.id,
     NoteFields.isImportant: this.isImportant? 1:0,
     NoteFields.number: this.number,
     NoteFields.title: this.title,
     NoteFields.description: this.description,
     NoteFields.createTime: this.createTime.toIso8601String(),
   };

   static Note fromJson(Map<String, Object?> json)
   => Note(
       id: json[NoteFields.id] as int?,
       isImportant: json[NoteFields.isImportant] == 1? true:false,
       number: json[NoteFields.number] as int,
       title: json[NoteFields.title] as String,
       description: json[NoteFields.description] as String ,
       createTime: DateTime.parse(json[NoteFields.createTime] as String),
   );

   Note copy(
       {
         int? id,
         bool? isImportant,
         int? number,
         String? title,
         String? description,
         DateTime? createTime}) =>

       Note(
          id: id?? this.id,
           isImportant: isImportant??this.isImportant,
           number: number??this.number,
           title:title??this.title,
           description: description??this.description,
           createTime: createTime??this.createTime) ;
}


