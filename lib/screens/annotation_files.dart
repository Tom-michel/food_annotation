
import 'package:food_annotation/screens/paint.dart';

class Annotations{
  late String image;
  List <ResizebleWidget> annotations = [];
  Annotations({
    required this.image,
    required this.annotations,
  });

  factory Annotations.fromJson(Map<String, dynamic> json) =>
      Annotations(
          image: json["image"],
          annotations: json["annotations"] as List<ResizebleWidget>
      );

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> labels = [];
    annotations.forEach((element) {
      labels.add(element.toJson());
    });
    return {
      "image": image,
      "annotations": labels,

    };
  }


}
