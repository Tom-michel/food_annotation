import 'package:flutter/material.dart';
import 'package:food_annotation/config/config.dart';
import 'package:food_annotation/screens/views/home_view.dart';
import 'package:food_annotation/screens/views/recognition.dart';
import 'package:food_annotation/screens/views/list_view.dart';


class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentIndex = 0;
  final screens = [
    CameraPage(),
    List_View(),
    Recognition(),
  ];

  int change() {
    return 1;
  }

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      child: Scaffold(
          body: screens[currentIndex],
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Config.colors.primaryColor,
            selectedItemColor: Config.colors.secondColor,
            type: BottomNavigationBarType.fixed,
            iconSize: 25,
            //selectedFontSize: 10,
            showUnselectedLabels: false,
            showSelectedLabels: false,
            //unselectedFontSize: 20,
            currentIndex: currentIndex,
            onTap: (index) => setState(() => currentIndex = index),
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.camera_alt), label: 'Camera'),
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.photo_outlined), label: 'Recognition')
            ],
          )),
    );
  }
}
