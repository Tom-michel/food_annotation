import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_annotation/config/config.dart';
import 'package:food_annotation/screens/home.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer timer = new Timer(new Duration(seconds: 15), () {
      print('hello world');
      Navigator.pushReplacementNamed(context, '/home');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Timer(const Duration(seconds: 5), () {
    //   Get.to(Home());
    // });
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/Page.png"),
            fit: BoxFit.cover,
          ),
        ),
        // decoration: BoxDecoration(
        //   gradient: LinearGradient(
        //     colors: [
        //       Color(0xFF1F8D2A).withOpacity(0.6),
        //       Color(0xFF1F8D2A).withOpacity(0.7),
        //       Color(0xFFFAE200).withOpacity(0.6),
        //       Color(0xFF1F8D2A).withOpacity(0.6),
        //       Color(0xFF1F8D2A).withOpacity(0.6),
        //     ],
        //     begin: Alignment.topCenter,
        //     end: Alignment.bottomCenter,
        //   ),
        // ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: SpinKitSpinningLines(
                  size: 50.0,
                  color: Colors.green,
                ),
              ),
              SizedBox(height: 2),
              Image.asset(
                Config.assets.plat,
                fit: BoxFit.contain,
              ),
              Image.asset(Config.assets.logo, fit: BoxFit.contain),
            ],
          ),
        ),
      ),
    );
  }
}
