
import 'dart:convert';

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:food_annotation/screens/annotation_files.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:food_annotation/screens/paint.dart';

class Labels{
  // static final  _mybox = Hive.box("label_box1");

  late String filename;
  Labels(String name){
    this.filename = name;
  }

  Future<File> get _findLocalPath async {
    Directory? directory ;
    try {
      if (Platform.isAndroid) {
        if (await _requestPermission(Permission.storage)) {
          directory = await getExternalStorageDirectory();
          String newPath = "/storage/emulated/0/Download";

          newPath = "$newPath/Food_annotation";
          directory = Directory(newPath);
        }
      }

      if (!await directory!.exists()) {
        await directory!.create(recursive: true);
      }
      if (await directory!.exists()) {
        List<String> file = filename.split('/');
        filename = file.last;
        if (filename.isEmpty){
          filename = "new_file";
        }
        File saveFile = File("${directory.path}/$filename.json");

        return saveFile;
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
    if (filename.isEmpty){
      filename = "new_file";
    }
    final path = await _localPath;
    return File('$path/$filename.json');


  }

  Future<File> saveImagePermanently(XFile imagePath) async{
    Directory? directory ;
    try {
      if (Platform.isAndroid) {
        if (await _requestPermission(Permission.storage)) {
          directory = await getApplicationDocumentsDirectory();
          String newPath = "/storage/emulated/0/Android/data/com.example.food_annotation";

          newPath = "$newPath/image";
          directory = Directory(newPath);
        }
      }

      if (!await directory!.exists()) {
        await directory!.create(recursive: true);
      }
      if (await directory!.exists()) {
        File tmp =  File(imagePath.path);

        String name = imagePath.name;
        print("before save we test ");
        bool exist = await File("${directory.path}/$name").exists();

        if (!exist) {
          print("img exist test");
          File saveFile = await tmp.copy("${directory.path}/$name");
          return saveFile;
        }

        return File("/storage/emulated/0/Android/data/com.example.food_annotation/image/image_picker8761451202276228687.jpg");
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
    return File(imagePath.path);
  }

  Future<bool> _requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> writeJsonFile(Annotations data) async {

    final file = await _findLocalPath;

    // Write the file
    return file.writeAsString(json.encode(data));
  }

  Future<Annotations?> readJsonFile() async {
    try {
      final file = await _findLocalPath;

      // Read the file
      final List<ResizebleWidget> anotationLoad = [];
      final Annotations data;
      var contents = await file.readAsString();
      Map<String, dynamic> dataPre = jsonDecode(contents);
      for(Map<String,dynamic> element in dataPre["annotations"]){
        ResizebleWidget data = ResizebleWidget.fromJson(element);
        anotationLoad.add(data);
      }
      data  = Annotations(image:dataPre["image"], annotations: anotationLoad);

      return data;
    } catch (e) {
      print(e);
      // If encountering an error, return 0
      return null;
    }
  }


}
